/* eslint-disable no-labels */
/* eslint-disable no-console */
import { CommonModule } from '@angular/common';
import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { ResourcesImplService } from 'app/services/resources-impl.service';

@Component({
  standalone: true,
  imports: [CommonModule],
  selector: 'jhi-rawjson-pane',
  templateUrl: './rawjson-pane.component.html',
  styleUrls: ['./rawjson-pane.component.scss'],
  providers: [ResourcesImplService],
})
export class RawjsonPaneComponent implements OnInit {
  @Input() chosenId: string;
  @Input() resourceType: string;
  fetchedRawData: string;
  constructor(private service: ResourcesImplService) {
    this.fetchedRawData = '';
    this.chosenId = '';
    this.resourceType = '';
    /*
      this.service.getJsonDetails('',this.resType,this.chosenId)
    this.service.getJsonDetails('','HostingNode',this.chosenId).subscribe(res => {
      this.fetchedRawData = res; });
      */
  }
  
  ngOnInit(): void {
	  console.debug("+++++before service++++");
	  let paramType : string;
	  console.debug("***resourceType: "+this.resourceType);
	  if(this.resourceType===''){
		  paramType = 'HostingNode';
	  }else{
		  paramType = this.resourceType;
	  }
      this.service.getJsonDetails('',paramType,this.chosenId).subscribe(res => {
      this.fetchedRawData = res;
    });
  }
}
