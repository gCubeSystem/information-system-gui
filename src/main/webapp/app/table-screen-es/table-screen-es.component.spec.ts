import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableScreenEsComponent } from './table-screen-es.component';

describe('TableScreenEsComponent', () => {
  let component: TableScreenEsComponent;
  let fixture: ComponentFixture<TableScreenEsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableScreenEsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TableScreenEsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
