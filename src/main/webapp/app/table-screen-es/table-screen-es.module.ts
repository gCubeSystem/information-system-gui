import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { RawjsonPaneComponent } from 'app/rawjson-pane/rawjson-pane.component';
import { TableScreenEsComponent } from './table-screen-es.component';



@NgModule({
  imports: [
    SharedModule,MatTableModule,MatIconModule,MatSortModule,MatPaginatorModule,RawjsonPaneComponent
  ],
  declarations:[TableScreenEsComponent],
  entryComponents: [TableScreenEsComponent],
  exports: [TableScreenEsComponent]
})
export class TableScreenEsModule { }
