/* eslint-disable @typescript-eslint/explicit-function-return-type */
/* eslint-disable no-console */
import {
  Component,
  Output,
  EventEmitter,
  OnInit,
  ViewChild,
  AfterViewInit,
  OnChanges,
  SimpleChanges,
  Input,
  QueryList,
} from '@angular/core';

import { MatTableDataSource } from '@angular/material/table';
import { MatSort} from '@angular/material/sort';
import { IContextNode } from 'app/services/i-context-node';
import { MatPaginator} from '@angular/material/paginator';
import { MatTab, MatTabGroup } from '@angular/material/tabs';
import { ITabbedEntity } from 'app/i-tabbed-entity';
import { IEService } from 'app/services/i-e-service';
import { ResourcesImplService } from 'app/services/resources-impl.service';


@Component({
  selector: 'jhi-table-screen-es',
  templateUrl: './table-screen-es.component.html',
  styleUrls: ['./table-screen-es.component.scss'],
  providers: [ResourcesImplService],
})

export class TableScreenEsComponent implements OnInit, AfterViewInit, OnChanges {
  //NB 'actions' CI DEVE ESSERE, altrimenti la tabella non viene visualizzata
  //displayedColumns: string[] = ['name', 'id', 'status', 'lastMod', 'artifact', 'group', 'version', 'endpoint','actions'];
  displayedColumns: string[] = ['name', 'id', 'status', 'lastMod',  'endpoint','actions'];
  dataFromService: IEService[];
  dataSource = new MatTableDataSource();
  tableDetail: IEService;

  @Input() myCtx: IContextNode; //fetching event from parent
  @Input() resourceType: string; 
  @Output() jsonEmitter = new EventEmitter<IEService>();
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  
    //per tabbed view
  @ViewChild(MatTab)
  public tabGroup: MatTabGroup | any;
  public tabNodes: QueryList<MatTab> | any;
  public closedTabs = [];
  public tabs: ITabbedEntity[] = [{ title: 'JSON View', content: '', type: 0, id: '' }];
  selectedIdx = 0;
  chosenIds: string[] = [];
  
  
  constructor(private myDataService: ResourcesImplService) {
    this.myCtx = {} as IContextNode;
    this.tableDetail = {} as IEService;
    this.dataFromService = [];
    this.sort = new MatSort();
    this.paginator = {} as MatPaginator;
    this.resourceType = '';
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }
  
  ngOnInit(): void {
    this.myDataService.fetchResourceImpls('',this.resourceType).subscribe(res => {
      this.dataFromService = res;
      this.dataSource.data = res;
      //this is to filter only on the 'name' property
      this.dataSource.filterPredicate = function (record:any,filter:string) {
       		return record.name.indexOf(filter)!==-1;
  		}
      //console.debug(res);
    });
  }
  
    ngOnChanges(changes: SimpleChanges): void {
    //  console.log('---NELLA ONCHANGES...' + this.myCtx.name);
    for (const propName in changes) {
      if (propName === 'myCtx') {
        const param = changes[propName];
        //TODO: questo è un tipo
        this.myCtx = <IContextNode>param.currentValue;
        //controllo che l'oggetto non sia vuoto
        if (Object.keys(this.myCtx).length !== 0) {
          //mt qui va passato il parametro ResourceType (preso dall'albero) al rest
          this.myDataService.fetchResourceImpls('',this.resourceType).subscribe(res => {
            this.dataFromService = res;
            this.dataSource.data = res;
          });
        }
      }
    }
  }
  
   applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    console.debug("++++++DATASOURCE:");
    console.debug(this.dataSource);
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  /*
 filterTable(): void {
    this.dataSource.filterPredicate = (data: IEService, filter: string): boolean => {
      return (
        data.name.toLocaleLowerCase().includes(filter)
      )
    }
  }
  */

 // per tabbed pane (versione con aggiunta dinamica)

  removeTab(index: number): void {
    this.tabs.splice(index, 1);
  }

  addTab(itemId: string): void {
    if (!this.chosenIds.includes(itemId)) {
      const newItem = {
        id: itemId,
        title: itemId.substring(0, 20) + '...',
        //TODO: content a regime è la stringa JSON
        content: itemId,
        type: 0,
      };
      this.selectedIdx++;
      this.chosenIds.push(itemId);
      this.tabs.push(newItem);
    }
  }

  closeTab(index: number): void {
    console.debug('---------');
    console.debug(index);
    console.debug('---IDs:');
    console.debug(this.chosenIds);
    console.debug(this.tabs[index].id);
    console.debug('++++++++++');
    const x = this.chosenIds.indexOf(this.tabs[index].id);
    if (x !== -1) {
      this.chosenIds.splice(x, 1);
    }

    // this.closedTabs.push(index);
    this.tabGroup.selectedIndex = this.tabs.length - 1;
    //this.chosenIds.indexOf();
    this.tabs.splice(index, 1);
  }
}
