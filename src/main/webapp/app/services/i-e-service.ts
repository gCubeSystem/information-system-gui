export interface IEService {
  id: string;
  lastMod: string;
  name: string;
  artifact: string;
  version: string;
  group: string
  status: string;
  endpoint: string;
  
}