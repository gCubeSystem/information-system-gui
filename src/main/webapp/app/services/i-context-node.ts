export interface IContextNode{
	name: string;
	id: string;
	parent: string;
	path: string;
}   
