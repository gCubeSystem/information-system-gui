/* eslint-disable no-console */
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { IHostingNode } from './i-hosting-node';
import { IEService } from './i-e-service';
import { ApplicationConfigService } from 'app/core/config/application-config.service';

@Injectable({
  providedIn: 'root'
})

export class ResourcesImplService {
//public isLoading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

 private loading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
 
  constructor(private http: HttpClient, private applicationConfigService: ApplicationConfigService) { }
 
	public isLoading():Observable<any>{
		 return this.loading;
	 }
  
  //TODO: paginate when APIs are ready
	fetchResourceImpls(ctx:string, type:string): Observable<any[]> {
			const resourceUrl = this.applicationConfigService.getEndpointFor('api/is/resourceinstances');
			let queryParams = new HttpParams();
			queryParams = queryParams.append("currentContext",ctx).append("resourceType",type);
			// eslint-disable-next-line @typescript-eslint/no-unsafe-return
			if(type==='HostingNode'){
				//console.debug("******GETTING HOSTING NODE*****");
				return this.http.get<IHostingNode[]>(resourceUrl,{params:queryParams});
			}
			if(type==='EService'){
				return this.http.get<IEService[]>(resourceUrl,{params:queryParams});
			}
			return new Observable<any[]>();
	  }
	  
	  

	  getJsonDetails(ctx:string, type:string, uid: string): Observable<string> {
		  const resourceUrl = this.applicationConfigService.getEndpointFor('api/is/resourcejson');
		 // const SERVICE_URL = appProperties.BASEURL_API+'is/resourcejson';
		  let queryParams = new HttpParams();
		  queryParams = queryParams.append("currentContext",ctx).append("resourceType",type).append("uid",uid);
    	return this.http.get<string>(resourceUrl,{params:queryParams});
  	}
  
}
