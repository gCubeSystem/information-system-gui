import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IResource} from './i-resource';
import { ApplicationConfigService } from 'app/core/config/application-config.service';

@Injectable({
  providedIn: 'root',
})

export class RestypesService {
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  constructor(private http: HttpClient, private applicationConfigService: ApplicationConfigService){}

 //TODO: pipe per gestione errori
 
  fetchAll(): Observable<IResource[]> {
    //return this.http.get<IResource[]>(appProperties.BASEURL_API+'is/resourcetypes');
    const resourceUrl = this.applicationConfigService.getEndpointFor('api/is/resourcetypes');
    return this.http.get<IResource[]>(resourceUrl);
  }


  
}
