/* eslint-disable no-console */
/* eslint-disable @typescript-eslint/explicit-function-return-type */
/* eslint-disable @typescript-eslint/prefer-nullish-coalescing */
/* eslint-disable @typescript-eslint/no-empty-function */
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class SpinnerLoadingService {

 //TODO: a regime inizializzare a true
 private state: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

    constructor() { 
	}
  
   getState():Observable<boolean>{
	  // console.debug("*******Getting observable state in SpinnerLoadingService");
	  return  this.state.asObservable();
  };
  
  setState(newState : boolean) : void{
	   // console.debug("******Setting state in SpinnerLoadingService: ");
	   // console.debug(newState);
	  //  console.debug("******");
		this.state.next(newState);
  };
  
}  
