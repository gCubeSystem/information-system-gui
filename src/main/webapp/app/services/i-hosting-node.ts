export interface IHostingNode {
  type:string;
  name: string;
  id: string;
  status: string;
  lastMod: string;
  avMemory: string;
  hdSpace: string;
}
