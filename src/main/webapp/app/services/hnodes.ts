import { IHostingNode } from "./i-hosting-node";

export const HNODES: IHostingNode[] = [{
  "type": "Hosting Node",
  "id": "id-ckan",
  "name": "ckan-d-d4s.d4science.org",
  "status": "certified",
  "lastmod": "Tue Jul 25 10:20:17 GMT 2023",
  "memavailable": "1% (137 MB)",
  "hdspace": "59 MB"
}, {
  "type": "Hosting Node",
  "id": "id-geoportal",
  "name": "geoportal.dev.int.d4science.net",
  "status": "certified",
  "lastmod": "Tue Jul 25 10:19:35 GMT 2023",
  "memavailable": "0% (18 MB)",
  "hdspace": "5 MB"
}, {
  "type": "Hosting Node",
  "id": "id-dataminer2",
  "name": "dataminer2.dev.int.d4science.net",
  "status": "certified",
  "lastmod": "Tue Jul 25 10:20:17 GMT 2023",
  "memavailable": "5% (430 MB)",
  "hdspace": "21 MB"
}, {
  "type": "Hosting Node",
  "id":"id-dataminer1",
  "name": "dataminer1.dev.d4science.org",
  "status": "certified",
  "lastmod": "Tue Jul 25 10:20:18 GMT 2023",
  "memavailable": "41% (2.035 MB)",
  "hdspace": "8 MB"
}]