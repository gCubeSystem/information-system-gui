/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable no-console */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IContextNode } from './i-context-node';
import { ApplicationConfigService } from 'app/core/config/application-config.service';

@Injectable({
  providedIn: 'root',
})
export class ContextsLoaderService {
  
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };
  
  constructor(private http: HttpClient, private applicationConfigService: ApplicationConfigService) {}
  
  
  fetchAll(): Observable<IContextNode[]> {
	//return this.http.get<IContextNode[]>(appProperties.BASEURL_API + 'is/allcontexts');
	const resourceUrl = this.applicationConfigService.getEndpointFor('api/is/allcontexts');
	return this.http.get<IContextNode[]>(resourceUrl);
  }
  
  
}
