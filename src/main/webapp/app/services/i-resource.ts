export interface IResource {
  name: string;
  id: string;
  //type: number; //TODO: codice da assegnare in constants per ogni tipo di risorsa
  children?: IResource[];
}
