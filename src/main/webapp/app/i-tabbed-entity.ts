export interface ITabbedEntity {
  id: string;
  title: string;
  content: string;
  //prevediamo contenuti di tipo diverso (quindi switch al momento della costruzione dei tab)
  type: number;
}
