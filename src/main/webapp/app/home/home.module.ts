import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { HOME_ROUTE } from './home.route';
import { HomeComponent } from './home.component';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { RscTreeModule } from 'app/rsc-tree/rsc-tree.module';
import { RawjsonPaneComponent } from 'app/rawjson-pane/rawjson-pane.component';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { MatTabsModule } from '@angular/material/tabs';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSelectFilterModule } from 'mat-select-filter';
import { TableScreenModule } from 'app/table-screen/table-screen.module';
import { TableScreenEsModule } from 'app/table-screen-es/table-screen-es.module';
import { AsyncPipe,  NgSwitchCase, NgSwitchDefault } from '@angular/common';
import { MatProgressBarModule } from '@angular/material/progress-bar';

@NgModule({
  imports: [SharedModule, 
  RouterModule.forChild([HOME_ROUTE]),
  NgbDropdownModule, 
  RscTreeModule,
  RawjsonPaneComponent,
  ClipboardModule,
  MatTabsModule,
  BrowserAnimationsModule,
  MatSelectFilterModule,
  MatProgressBarModule,
  TableScreenModule,
  TableScreenEsModule,
  AsyncPipe,
  NgSwitchCase,
  NgSwitchDefault],
  declarations: [HomeComponent],
  schemas:[CUSTOM_ELEMENTS_SCHEMA],
})
export class HomeModule {}
