/* eslint-disable @typescript-eslint/no-inferrable-types */
/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable no-var */
/* eslint-disable no-console */
/* eslint-disable @typescript-eslint/no-unnecessary-condition */
/* eslint-disable @typescript-eslint/restrict-plus-operands */
// eslint-disable-next-line @typescript-eslint/no-unsafe-return
import { Component, OnDestroy, OnInit, ViewChild,ElementRef, AfterContentInit, AfterViewInit } from '@angular/core';

import { LoginService } from 'app/login/login.service';
import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/auth/account.model';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Clipboard } from '@angular/cdk/clipboard'
import { Observable, Subject, Subscription, delay, map, startWith, takeUntil } from 'rxjs';
import { ContextsLoaderService } from 'app/services/contexts-loader.service';
import { IContextNode } from 'app/services/i-context-node';
import { AutofillValidator, instanceOfContext } from './autofill-validator';
import { SpinnerLoadingService } from 'app/services/spinner-loading.service';
//import { RscTreeComponent } from 'app/rsc-tree/rsc-tree.component';


@Component({
	selector: 'jhi-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss'],
	providers: [ContextsLoaderService],
})

export class HomeComponent implements OnInit, OnDestroy, AfterViewInit {
	account: Account | null = null;
	myContext: IContextNode | null = null;
	//stringCtxs: string[];
	allCtxs: IContextNode[];
  	public filteredContexts: Observable<IContextNode[]> | null = null;
  //public filteredContextsStr: Observable<string[]>| undefined;
  	chooseContextForm: FormGroup | any;
  	resType: string;
  	isLoading: boolean = false;
  	subscriptions: Subscription[] = [];
  	
 	//@ViewChild('leftTree') leftTree: ElementRef | undefined;
	
	constructor(
		private accountService: AccountService, 
		private loginService: LoginService,
		private ctxLoaderService : ContextsLoaderService,
		private progressService : SpinnerLoadingService,
		private fb: FormBuilder, 
		private clipboard: Clipboard) {
			//initialise on HostingNodes
		this.resType="HostingNode";
		this.allCtxs = [];
  		}
  		
  		
	  get namefield(): any {
	    // eslint-disable-next-line @typescript-eslint/no-unsafe-return
	    return this.chooseContextForm.get('namefield');
	  }
	  get uidfield(): any {
	    // eslint-disable-next-line @typescript-eslint/no-unsafe-return
	    return this.chooseContextForm.get('uidfield');
	  }

	ngOnInit(): void {
			//to identify users and their account
			this.accountService.identity().subscribe(account => (this.account = account));
				  this.chooseContextForm = this.fb.group({
				      namefield: ['', [Validators.required,AutofillValidator]],
				      uidfield: [''],
				    });
			 
				  // per la form dei contesti
			this.ctxLoaderService.fetchAll().subscribe(res => {
		      this.allCtxs = res;
		    });
		    
		   this.subscriptions.push(this.progressService.getState()
			.pipe()
		      .subscribe(res => {
		        this.isLoading = res;
		      }));
	}
	
	ngAfterViewInit(): void {
	    this.filteredContexts = this.chooseContextForm.get('namefield').valueChanges.pipe(  
						startWith(''),
			      		map(ctx => (ctx ? this.filterContexts(ctx/*,contextInput*/) : this.allCtxs.slice()))
			    	);
	}
	
	/*
	getContexts(): string[] {
		if (this.account != null && this.account.resourceAccessDTO != null) {
			// eslint-disable-next-line no-console
			var sc=this.account.resourceAccessDTO.resourceAccess;
          	var contexts=Array.from(Object.keys(sc));
          	contexts=contexts.filter(element=>
          		element!==null&&element.startsWith("%2F"));
          	contexts=contexts.sort();
          	// eslint-disable-next-line no-console
          	console.log(contexts);
          	return contexts;
			
		} else {
			return [];
		}
	}
	*/
	
	//TODO: CHECK WHY UNSUBSCRIBE DOES NOT WORK
		ngOnDestroy(): void {
			//alert(this.subscriptions.length);
 			this.subscriptions.forEach(sub => sub.unsubscribe());
	}
 
	//mettere ANY come tipo dell'argomento per evitare errore TypeScript nella map!!
	filterContexts(/*ctx: IContextNode,*/ item: any): IContextNode[] {
		if(!instanceOfContext(item)){
			return this.allCtxs.filter(ctx => ctx.name.toLowerCase().includes(item.toLowerCase()));
		}else{
			return [];
		}
  }
  

  displayFn(ctx: IContextNode): string {
    //return ctx.name ? ctx.name + ' | ' + ctx.id : '';
    return ctx.name;
  }


  copyUid(val: any): void {
    if (val instanceof FormControl) {
      this.clipboard.copy(val.getRawValue().id);
    } else {
      //this.clipboard.copy('invalid value');
      this.clipboard.copy('invalid value');
    }
  }
  
	login(): void {
		this.loginService.login();
	}
	
	buildTableData(newType:string):void{
		this.resType = newType;
		console.log("++++++resType..."+this.resType);
	}
}
