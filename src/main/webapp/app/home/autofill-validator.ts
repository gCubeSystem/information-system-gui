/* eslint-disable no-console */
/* eslint-disable @typescript-eslint/no-unnecessary-condition */
import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";


export function instanceOfContext(context: any): boolean{
	// eslint-disable-next-line no-labels
    const res = !!context // truthy
    && typeof context !== 'string' // Not just string input in the autocomplete
    && 'name' in context; // Has some qualifying property of context type
    console.debug(res);
    return res;
}


export const AutofillValidator: ValidatorFn = 
(control: AbstractControl): ValidationErrors | null =>!instanceOfContext(control?.value) ? { matchRequired: true } : null;

