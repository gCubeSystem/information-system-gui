export interface ITestDialog {
  status: string;
}

export class TestDialog implements ITestDialog {
  constructor(public status: string) {}
}

export function getStatus(testDialog: ITestDialog): string {
  return testDialog.status;
}
