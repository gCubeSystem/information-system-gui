import { TestBed } from '@angular/core/testing';
import { HttpErrorResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { TestDialog, ITestDialog } from './testdialog.model';

import { TestDialogService } from './testdialog.service';

describe('TestDialog Service', () => {
  let service: TestDialogService;
  let httpMock: HttpTestingController;
  let expectedResult: ITestDialog | boolean | number | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(TestDialogService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  describe('Service methods', () => {
    it('should return test dialog', () => {
      service.query().subscribe(received => {
        expectedResult = received.body;
      });

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([]);
      expect(expectedResult).toEqual([{ status: 'Hello'}]);
    });

    it('should propagate not found response', () => {
      service.query().subscribe({
        error: (error: HttpErrorResponse) => (expectedResult = error.status),
      });

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush('Internal Server Error', {
        status: 500,
        statusText: 'Inernal Server Error',
      });
      expect(expectedResult).toEqual(500);
    });

  });
});
