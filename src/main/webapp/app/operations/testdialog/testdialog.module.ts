
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { TestDialogComponent } from './testdialog.component';
import { testDialogRoute } from './testdialog.route';

@NgModule({
  imports: [SharedModule, RouterModule.forChild(testDialogRoute)],
  declarations: [TestDialogComponent],
  entryComponents: [TestDialogComponent],
})
export class TestDialogModule {}