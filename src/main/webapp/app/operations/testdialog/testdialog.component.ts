import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

import { TestDialogService } from './testdialog.service';
import { HttpErrorResponse } from '@angular/common/http';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/auth/account.model';


@Component({
	selector: 'jhi-testdialog',
	templateUrl: './testdialog.component.html',
	styleUrls: ['testdialog.scss'],
})
export class TestDialogComponent implements OnInit {
	account: Account | null = null;
	year: number;
	month: number;

	m!: NgbModalRef;
	@ViewChild('content', { static: true }) content!: ElementRef;

	constructor(private accountService: AccountService, private modalService: NgbModal, private testDialogService: TestDialogService) {
		const date: Date = new Date();
		this.year = date.getFullYear();
		this.month = date.getMonth() + 1;
	}


	ngOnInit(): void {
		this.accountService.identity().subscribe(account => (this.account = account));
	}




	private openLoading(): void {
		this.m = this.modalService.open(this.content, { size: 'sm', centered: true, backdrop: 'static', keyboard: false });
	}

	private closeLoading(): void {
		this.m.close();
	}

	runTest(): void {
		this.openLoading();
		if (this.year == null || this.year <= 0) {
			// eslint-disable-next-line no-console
			console.log('Select the year');
			this.closeLoading();
			return;
		}
		if (this.month == null || this.month <= 0) {
			// eslint-disable-next-line no-console
			console.log('Select the month');
			this.closeLoading();
			return;
		}


		if (this.accountService.isAuthenticated()) {
			// eslint-disable-next-line no-console
			console.log(this.account);
			this.testDialogService.query().subscribe({
				complete: () => { this.onSuccess() },
				error: error => { this.onError(error) }
			}
			);
		} else {
			// eslint-disable-next-line no-console
			console.log('Not Autenticated');
			this.closeLoading();
			return;
		}
	}

	private onSuccess(): void {
		this.closeLoading();
		// eslint-disable-next-line no-console
		console.log('Success');
	}

	private onError(error: HttpErrorResponse): void {
		this.closeLoading();
		// eslint-disable-next-line no-console
		console.log('Error');
		// eslint-disable-next-line no-console
		console.log(error);
	}
}
