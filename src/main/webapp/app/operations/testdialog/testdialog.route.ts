import { Routes } from '@angular/router';

import { TestDialogComponent } from './testdialog.component';

export const testDialogRoute: Routes = [
  {
    path: '',
    component: TestDialogComponent,
    data: {
      pageTitle: 'Rendicontazione',
    },
  },
];