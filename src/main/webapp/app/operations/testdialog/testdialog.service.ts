import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { ITestDialog } from './testdialog.model';

@Injectable({ providedIn: 'root' })
export class TestDialogService {
  private resourceUrl = this.applicationConfigService.getEndpointFor('api/testdialog/test');

  constructor(private http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  query(): Observable<HttpResponse<ITestDialog>> {
	// eslint-disable-next-line no-console
	console.log(this.resourceUrl);
	return this.http.get<ITestDialog>(this.resourceUrl, { observe: 'response' });
  }
  
}
