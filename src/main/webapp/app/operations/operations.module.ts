import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
	imports: [
		RouterModule.forChild([
			/* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
			{
				path: 'testdialog',
				loadChildren: () => import('./testdialog/testdialog.module').then(m => m.TestDialogModule),
				data: {
					pageTitle: 'Test Dialog',
				},
			},
			{
				path: 'testcytoscape',
				loadChildren: () => import('./testcytoscape/testcytoscape.module').then(m => m.TestCytoscapeModule),
				data: {
					pageTitle: 'Test Cytoscape',
				},
			}
		]),
	],
})
export class OperationsModule { }