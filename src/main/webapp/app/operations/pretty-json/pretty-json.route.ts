import { Routes } from '@angular/router';
import { PrettyJsonComponent } from './pretty-json.component';


export const PRETTYJSON_ROUTE: Routes = [{
  path: '',
  component: PrettyJsonComponent,
  data: {
    pageTitle: 'Pretty Print of fetched JSON',
  },
}];
