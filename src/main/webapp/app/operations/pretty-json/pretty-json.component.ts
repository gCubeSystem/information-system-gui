import { Component, OnInit } from '@angular/core';
import { ContextsLoaderService } from 'app/services/contexts-loader.service';
import { IContextNode } from './i-context-node';



@Component({
  selector: 'jhi-pretty-json',
  templateUrl: './pretty-json.component.html',
  styleUrls: ['./pretty-json.component.scss'],
  providers: [ContextsLoaderService],
  
})

export class PrettyJsonComponent implements OnInit {
	
 fetchedData?: IContextNode[];
 fetchedRawData?: string;

  constructor(private myService:ContextsLoaderService) { }

  ngOnInit(): void {
	  this.myService.getData().subscribe(res=>{
		  this.fetchedData = res
	  });
	  this.myService.getRawData().subscribe(res=>{
		  this.fetchedRawData = res
	  });
  }

}
