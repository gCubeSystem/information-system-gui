import { NgModule } from '@angular/core';
import { PrettyJsonComponent } from './pretty-json.component';
import { SharedModule } from 'app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { PRETTYJSON_ROUTE } from './pretty-json.route';



@NgModule({
  declarations: [
    PrettyJsonComponent
  ],
  imports: [
    SharedModule,
    RouterModule.forChild(PRETTYJSON_ROUTE)
  ],
  entryComponents: [PrettyJsonComponent],
})
export class PrettyJsonModule { }


