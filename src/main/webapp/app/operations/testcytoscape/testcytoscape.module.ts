
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { TestCytoscapeComponent } from './testcytoscape.component';
import { testCytoscapeRoute } from './testcytoscape.route';

@NgModule({
  imports: [SharedModule, RouterModule.forChild(testCytoscapeRoute)],
  declarations: [TestCytoscapeComponent],
  entryComponents: [TestCytoscapeComponent],
})
export class TestCytoscapeModule {}