import { Component, OnInit } from '@angular/core';
import cytoscape from 'cytoscape';

@Component({
	selector: 'jhi-testcytoscape',
	templateUrl: './testcytoscape.component.html',
	styleUrls: ['testcytoscape.scss'],
})
export class TestCytoscapeComponent implements OnInit {
	cy: any;

	constructor() { }

	ngOnInit(): void {
		this.cy = cytoscape({
			container: document.getElementById('cy'),
			elements: [
				{ data: { id: 'a', label: 'Pippo' } },
				{ data: { id: 'b', label: 'Pluto' } },
				{
					data: {
						id: 'ba',
						source: 'b',
						target: 'a'
					}
				}],
			style: [
				{
					selector: 'node',
					style: {
						shape: 'hexagon',
						'background-color': '#166',
						label: 'data(label)'
					}
				},
				{
					selector: 'edge',
					style: {
						'width': 3,
						'line-color': '#c11',
						'target-arrow-color': '#c11',
						'target-arrow-shape': 'triangle',
						'curve-style': 'bezier'
					}
				}
			]
		});
		for (var i = 0; i < 10; i++) {
			this.cy.add({
				data: { id: 'node' + i, label: 'node' + i }, position: { x: 10 + (i * 10), y: 0 }
			}
			);
			var source = 'node' + i;
			this.cy.add({
				data: {
					id: 'edge' + i,
					source: source,
					target: (i % 2 == 0 ? 'a' : 'b')
				}
			});
		}

		this.cy.layout({
			name: 'breadthfirst',
			fit: true, // whether to fit the viewport to the graph
			directed: false, // whether the tree is directed downwards (or edges can point in any direction if false)
			padding: 10, // padding on fit
			roots: '#a'
		}).run();


	}

}