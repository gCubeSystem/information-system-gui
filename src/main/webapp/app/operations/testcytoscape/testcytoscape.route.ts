import { Routes } from '@angular/router';

import { TestCytoscapeComponent } from './testcytoscape.component';

export const testCytoscapeRoute: Routes = [
  {
    path: '',
    component: TestCytoscapeComponent,
    data: {
      pageTitle: 'Test Cytoscape',
    },
  },
];