import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatSortModule } from '@angular/material/sort';
import { TableScreenComponent } from './table-screen.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { RawjsonPaneComponent } from 'app/rawjson-pane/rawjson-pane.component';



@NgModule({
  imports: [
    SharedModule,MatTableModule,MatIconModule,MatSortModule,MatPaginatorModule,RawjsonPaneComponent
  ],
  declarations:[TableScreenComponent],
  entryComponents: [TableScreenComponent],
  exports: [TableScreenComponent]
})

export class TableScreenModule {
	
	
 }
