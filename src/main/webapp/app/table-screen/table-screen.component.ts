/* eslint-disable no-console */
import {
  Component,
  Output,
  EventEmitter,
  OnInit,
  ViewChild,
  AfterViewInit,
  OnChanges,
  SimpleChanges,
  Input,
  QueryList,
} from '@angular/core';

import { MatTableDataSource } from '@angular/material/table';
import { MatSort} from '@angular/material/sort';
import { IContextNode } from 'app/services/i-context-node';
import { MatPaginator} from '@angular/material/paginator';
import { MatTab, MatTabGroup } from '@angular/material/tabs';
import { ITabbedEntity } from 'app/i-tabbed-entity';
import { IHostingNode } from 'app/services/i-hosting-node';
import { ResourcesImplService } from 'app/services/resources-impl.service';



@Component({
  selector: 'jhi-table-screen',
  templateUrl: './table-screen.component.html',
  styleUrls: ['./table-screen.component.scss'],
  providers: [ResourcesImplService],
})

export class TableScreenComponent implements OnInit, AfterViewInit, OnChanges {
  //NB 'actions' CI DEVE ESSERE, altrimenti la tabella non viene visualizzata
  displayedColumns: string[] = ['name', 'id', 'status', 'lastMod', 'avMemory', 'hdSpace', 'actions'];
  dataFromService: IHostingNode[];
  dataSource = new MatTableDataSource();
  tableDetail: IHostingNode;

  @Input() myCtx: IContextNode; //fetching event from parent
  @Input() resourceType: string; 
  @Output() jsonEmitter = new EventEmitter<IHostingNode>();
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  

  //per tabbed view
  @ViewChild(MatTab)
  public tabGroup: MatTabGroup | any;
  public tabNodes: QueryList<MatTab> | any;
  public closedTabs = [];
  public tabs: ITabbedEntity[] = [{ title: 'JSON View', content: '', type: 0, id: '' }];
  selectedIdx = 0;
  chosenIds: string[] = [];
  ////////// fine tabbed view

  constructor(private myDataService: ResourcesImplService) {
    this.myCtx = {} as IContextNode;
    this.tableDetail = {} as IHostingNode;
    this.dataFromService = [];
    this.sort = new MatSort();
    this.paginator = {} as MatPaginator;
    this.resourceType = '';
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(): void {
    this.myDataService.fetchResourceImpls('',this.resourceType).subscribe(res => {
      this.dataFromService = res;
      this.dataSource.data = res;
      //this is to filter only on the 'name' property
      this.dataSource.filterPredicate = function (record:any,filter:string) {
       		return record.name.indexOf(filter)!==-1;
  		}
  		/*
  		this.dataSource.filterPredicate =
      		(data: IHostingNode, filter: string) => !filter || data.level == filter;
      		*/
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    //  console.log('---NELLA ONCHANGES...' + this.myCtx.name);
    for (const propName in changes) {
      if (propName === 'myCtx') {
        const param = changes[propName];
        this.myCtx = <IContextNode>param.currentValue;
        //controllo che l'oggetto non sia vuoto
        if (Object.keys(this.myCtx).length !== 0) {
          this.myDataService.fetchResourceImpls('',this.resourceType).subscribe(res => {
            this.dataFromService = res;
            this.dataSource.data = res;
          });
        }
      }
    }
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  // for the dynamic tabbed pane

  removeTab(index: number): void {
    this.tabs.splice(index, 1);
  }

  addTab(itemId: string): void {
    if (!this.chosenIds.includes(itemId)) {
      const newItem = {
        id: itemId,
        title: itemId.substring(0, 20) + '...',
        //TODO: content a regime è la stringa JSON
        content: itemId,
        type: 0,
      };
      this.selectedIdx++;
      this.chosenIds.push(itemId);
      this.tabs.push(newItem);
    }
  }

  closeTab(index: number): void {
    console.debug('---------');
    console.debug(index);
    console.debug('---IDs:');
    console.debug(this.chosenIds);
    console.debug(this.tabs[index].id);
    console.debug('++++++++++');
    const x = this.chosenIds.indexOf(this.tabs[index].id);
    if (x !== -1) {
      this.chosenIds.splice(x, 1);
    }

    // this.closedTabs.push(index);
    this.tabGroup.selectedIndex = this.tabs.length - 1;
    //this.chosenIds.indexOf();
    this.tabs.splice(index, 1);
  }
  /*
 emitDataItem(node: IHostingNode):void {
	 this.jsonEmitter.emit(node);
  }
  */
  
}
