/* eslint-disable no-console */
import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,  
  HttpResponse
} from '@angular/common/http';
import { Observable, finalize, tap } from 'rxjs';
import { SpinnerLoadingService } from 'app/services/spinner-loading.service';

@Injectable()
export class SpinnercontrolInterceptor implements HttpInterceptor {

	   
  constructor(private myService: SpinnerLoadingService) {
  }

/*
	  intercept(request: HttpRequest<any>,next: HttpHandler): Observable<HttpEvent<any>> {		
	    	//console.log('********Outgoing HTTP request', request);
	    	if(request.url.includes('resourceinstances')){
			        finalize(() => {
				     console.debug("***setting to false");
			          this.myService.setState(false);
			        })
			        return next.handle(request);
			}
				return next.handle(request);
	}
	*/
	
	intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
			    const started = Date.now();
			    let ok: string;
			    // extend server response observable with logging
			   if(request.url.includes('resourceinstances')){
				this.myService.setState(true);
			    return next.handle(request)
			      .pipe(
			        tap({
			          // Succeeds when there is a response; ignore other events
			          next: (event) => (ok = event instanceof HttpResponse ? 'succeeded' : ''),
			          // Operation failed; error is an HttpErrorResponse
			          error: (_error) => (ok = 'failed')
			        }),
			        // Log when response observable either completes or errors
			        finalize(() => {
						/*
			          const elapsed = Date.now() - started;
			          const msg = `${request.method} "${request.urlWithParams}"
			             ${ok} in ${elapsed} ms.`;
			          console.debug("***** MSG INIZIO ******");
			          console.debug(msg);
			          */
			          this.myService.setState(false);
			        })
			      );
			  }else{
				  return next.handle(request);
			  }
		}
}

  