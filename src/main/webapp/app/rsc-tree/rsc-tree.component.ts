    /* eslint-disable no-console */
import { Component , OnInit, Output,EventEmitter } from '@angular/core';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { NestedTreeControl } from '@angular/cdk/tree';
import { RestypesService } from 'app/services/restypes.service';
import { IResource } from 'app/services/i-resource';


@Component({
  selector: 'jhi-rsc-tree',
  templateUrl: './rsc-tree.component.html',
  styleUrls: ['./rsc-tree.component.scss'],
  providers: [RestypesService],
})

export class RscTreeComponent implements OnInit{
  nestedTreeControl = new NestedTreeControl<IResource>(node => node.children);
  nestedDataSource = new MatTreeNestedDataSource<IResource>();
  statusClass = 'not-active';
  
 @Output() public resourceTypeEm = new EventEmitter<string>();

 constructor(private rtService:RestypesService) {
  }
  
  ngOnInit(): void {
	  this.rtService.fetchAll().subscribe(res => {
      this.nestedDataSource.data = res;
      this.nestedTreeControl.dataNodes = this.nestedDataSource.data;
      this.nestedTreeControl.expandAll();
       });
		}
		
		
hasNestedChild(_: number, node: IResource): boolean {
    if (node.children == null) {
      return false;
    } else {
      return node.children.length > 0;
    }
  }

  //TODO: InformationSystemResourceClient shoud pass a code, not a name! 
	onClickNodeTree(node:IResource):void{
		//this.setActiveClass();
		this.resourceTypeEm.emit(node.name);
	}
	
 setActiveClass():void{
    this.statusClass = 'active';
  }
	
}
