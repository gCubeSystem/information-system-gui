import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { MatIconModule } from '@angular/material/icon';
import { MatTreeModule } from '@angular/material/tree';
import { RscTreeComponent } from './rsc-tree.component';


@NgModule({
  imports: [SharedModule, MatIconModule,MatTreeModule],
  declarations: [
    RscTreeComponent
  ],
 entryComponents: [RscTreeComponent],
  exports: [RscTreeComponent]
})
export class RscTreeModule { }
