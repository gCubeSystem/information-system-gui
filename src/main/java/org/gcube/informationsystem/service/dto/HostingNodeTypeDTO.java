package org.gcube.informationsystem.service.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HostingNodeTypeDTO {
	private String id;
	private String name;
	private HostingNodeTypeDTO[] children;
	
	/*
	 *   
  type:string;
  name: string;
  id: string;
  status: string;
  lastmod: string;
  memavailable: string;
  hdspace: string;
	 */
}
