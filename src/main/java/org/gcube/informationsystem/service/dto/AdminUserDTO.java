package org.gcube.informationsystem.service.dto;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Set;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.gcube.informationsystem.config.Constants;

/**
 * A DTO representing a user, with his authorities.
 */
public class AdminUserDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;

	private Set<String> authorities;

	@NotBlank
	@Pattern(regexp = Constants.LOGIN_REGEX)
	@Size(min = 1, max = 50)
	private String login;

	@Size(max = 50)
	private String firstName;

	@Size(max = 50)
	private String lastName;

	@Email
	@Size(min = 5, max = 254)
	private String email;

	@Size(max = 256)
	private String imageUrl;

	private boolean activated = false;

	@Size(min = 2, max = 10)
	private String langKey;

	private RealmAccessDTO realmAccessDTO;

	private ResourceAccessDTO resourceAccessDTO;

	// private Set<String> authorities;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Set<String> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(Set<String> authorities) {
		this.authorities = authorities;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public boolean isActivated() {
		return activated;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}

	public String getLangKey() {
		return langKey;
	}

	public void setLangKey(String langKey) {
		this.langKey = langKey;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public RealmAccessDTO getRealmAccessDTO() {
		return realmAccessDTO;
	}

	public void setRealmAccessDTO(RealmAccessDTO realmAccessDTO) {
		this.realmAccessDTO = realmAccessDTO;
	}

	public ResourceAccessDTO getResourceAccessDTO() {
		return resourceAccessDTO;
	}

	public void setResourceAccessDTO(ResourceAccessDTO resourceAccessDTO) {
		this.resourceAccessDTO = resourceAccessDTO;
	}

	@Override
	public String toString() {
		return "AdminUserDTO [id=" + id + ", authorities=" + authorities + ", login=" + login + ", firstName="
				+ firstName + ", lastName=" + lastName + ", email=" + email + ", imageUrl=" + imageUrl + ", activated="
				+ activated + ", langKey=" + langKey + ", realmAccessDTO=" + realmAccessDTO + ", resourceAccessDTO="
				+ resourceAccessDTO + "]";
	}

}
