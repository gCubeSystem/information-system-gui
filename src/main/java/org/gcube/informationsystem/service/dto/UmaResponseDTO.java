package org.gcube.informationsystem.service.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class UmaResponseDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private boolean upgraded;
	private String access_token;
	private int expires_in;
	private int refresh_expires_in;
	private String refresh_token;
	private String token_type;
	private int not_before_policy;	
	
}
