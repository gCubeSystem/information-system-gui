package org.gcube.informationsystem.service.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HostingNodeDTO {
	private String id;
	private String lastMod;
	private String name;
	private String status;
	private String avMemory;
	private String hdSpace;
}
