package org.gcube.informationsystem.service.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EServiceDTO {
	private String id;
	private String lastMod;
	private String name;
	private String artifact;
	private String version;
	private String group;
	private String status;
	private String endpoint;
}
