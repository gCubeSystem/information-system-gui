package org.gcube.informationsystem.service.dto;

import java.util.ArrayDeque;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ResourceTypeDTO {

	private static final Logger log = LoggerFactory.getLogger(ResourceTypeDTO.class);
	 //TODO: vedi se a regime può servire ID
	  private String name;
	  private String id;
	  private String fatherId;
	  public String getFatherId() {
		return fatherId;
	}

	public void setFatherId(String fatherId) {
		this.fatherId = fatherId;
	}


	private List<ResourceTypeDTO> children;
	 
	  public ResourceTypeDTO(String name, String id) {
	    this.name = name;
	    this.id = id;
	    this.children = new LinkedList<>();
	  }
	 
	  public void addChild(ResourceTypeDTO childNode) {
	    this.children.add(childNode);
	  }
	 
	  public void showTreeNodes() {
		  visitNodes(this);
	  }
	 
	  public String getName() {
		  return name;
	  }
	  
	  public String getId() {
		  return id;
	  }
	  
	  public void setName(String name) {
		  this.name = name;
	  }
	  
	  public void setId(String id) {
	    this.id = id;
	  }
	 
	  public List<ResourceTypeDTO> getChildren() {
	    return children;
	  }
	  
	  
	  public void visitNodes(ResourceTypeDTO node) {
	        Queue<ResourceTypeDTO> queue = new ArrayDeque<>();
	        queue.add(node);

	        ResourceTypeDTO currentNode;
	        Set<ResourceTypeDTO> alreadyVisited = new HashSet<>();
	        log.debug("Visited nodes: ");
	        while (!queue.isEmpty()) {
	            currentNode = queue.remove();
	            log.debug(currentNode.getName() + " | ");

	            alreadyVisited.add(currentNode);
	            queue.addAll(currentNode.getChildren());
	            queue.removeAll(alreadyVisited);
	        }
	    }
	}

