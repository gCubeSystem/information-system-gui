package org.gcube.informationsystem.service.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResourceAccessDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private LinkedHashMap<String, ArrayList<String>> resourceAccess;

}
