package org.gcube.informationsystem.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ContextDTO {
	private String id;
	private String name;
	private String parent;
	private String path;
}
