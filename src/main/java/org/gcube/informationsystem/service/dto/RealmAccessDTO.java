package org.gcube.informationsystem.service.dto;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class RealmAccessDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private ArrayList<String> roles;

}
