package org.gcube.informationsystem.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutorService;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.node.ArrayNode;
import org.gcube.common.authorization.utils.manager.SecretManager;
import org.gcube.common.authorization.utils.manager.SecretManagerProvider;
import org.gcube.common.authorization.utils.secret.JWTSecret;
import org.gcube.common.authorization.utils.secret.Secret;
import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.contexts.reference.entities.Context;
import org.gcube.informationsystem.contexts.reference.relations.IsParentOf;
import org.gcube.informationsystem.model.knowledge.ModelKnowledge;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.properties.Metadata;
import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.informationsystem.resourceregistry.api.exceptions.entities.resource.ResourceAvailableInAnotherContextException;
import org.gcube.informationsystem.resourceregistry.api.rest.AccessPath;
import org.gcube.informationsystem.resourceregistry.api.rest.InstancePath;
import org.gcube.informationsystem.resourceregistry.client.ResourceRegistryClient;
import org.gcube.informationsystem.resourceregistry.client.ResourceRegistryClientFactory;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.informationsystem.service.dto.ContextDTO;
import org.gcube.informationsystem.service.dto.ResourceTypeDTO;
import org.gcube.informationsystem.tree.Node;
import org.gcube.informationsystem.tree.NodeElaborator;
import org.gcube.informationsystem.tree.Tree;
import org.gcube.informationsystem.types.knowledge.TypeInformation;
import org.gcube.informationsystem.types.reference.Type;
import org.gcube.informationsystem.types.reference.entities.ResourceType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.deser.std.UUIDDeserializer;

/**
 * Service class for managing InformationSystem resources.
 */
@Service
public class InformationSystemService {

	private static final Logger log = LoggerFactory.getLogger(InformationSystemService.class);
	private ArrayList<String> fatherIds;
	
	public void setUma(String umaToken) throws Exception {
		log.debug("Set Uma: [umaToken={}]",umaToken);
		SecretManagerProvider.instance.reset();
		SecretManager secretManager = new SecretManager();
		Secret secret = new JWTSecret(umaToken);
		secretManager.addSecret(secret);
		secretManager.set();
		SecretManagerProvider.instance.set(secretManager);
		
	}
	
	
	public String executeQueryTemplate(String name) throws Exception {
		ResourceRegistryClient resourceRegistryClient= ResourceRegistryClientFactory.create();
		try {
			//String res1 = resourceRegistryClient.runQueryTemplate("IS-Monitoring-All-HostingNode");
			String res = resourceRegistryClient.runQueryTemplate(name);
			log.debug("***IS-Monitoring-All-HostingNode***:");
			log.debug(res);
			/*
			String res2 = resourceRegistryClient.runQueryTemplate("IS-Monitoring-All-EService");
			log.debug("***IS-Monitoring-All-EService***:");
			log.debug(res2);
			*/
			return res;
		}catch(Exception e) {
			log.error("ERRORE IN GETRESOURCETYPES");
			throw e;
		}
		
	}
	
	private String extractPath(Context ctx) {
		String res = "/"+ctx.getName();
		if(ctx.getParent()==null) {
			return res;
		}
			Context dad = ctx.getParent().getSource();
			res = "/"+ctx.getParent().getSource().getName()+res;
			if(dad.getParent()==null) {
				return res;
				}
			Context gdad = dad.getParent().getSource();
				res = "/"+dad.getParent().getSource().getName()+res;
				if(gdad.getParent()==null) {
					return res;
				}
			Context g2dad = gdad.getParent().getSource();
					res = "/"+gdad.getParent().getSource().getName()+res;
					if(g2dad.getParent()==null) {
						return res;
					}
					return res;
		}
	
	
	
	public List<ContextDTO> getAllContexts() throws Exception {
		ArrayList<ContextDTO> res = new ArrayList<ContextDTO>();
		//log.debug("GetAllContext: [rootCtx=]",rootCtx);
		ResourceRegistryClient resourceRegistryClient= ResourceRegistryClientFactory.create();
		List<Context>contexts=resourceRegistryClient.getAllContext();
		for(Context ctx:contexts) {
			String pnt = " - "; //for the root
			//TODO: da dove tiro fuori il path?
			ContextDTO dto = new ContextDTO();
			dto.setId(ctx.getID().toString()); 
			dto.setName(ctx.getName());
			if(ctx.getParent()!=null) {
				if(ctx.getParent().getSource()!=null) {
					pnt = ctx.getParent().getSource().getName();
				}
			}
			dto.setParent(pnt);
			dto.setPath(extractPath(ctx));
			res.add(dto);
		}
		log.debug("AllContexts: {}",contexts);
		return res;
	}
	
	/*
	 * per prendere tutti i tipi (albero a sin)
	 */
	public List<Type> getResourceTypes() throws Exception {
		
		String currentCtx = SecretManagerProvider.instance.get().getContext();
		log.debug("getResourceTypes : [currentCtx=]",currentCtx);
		ResourceRegistryClient resourceRegistryClient= ResourceRegistryClientFactory.create(currentCtx);
		List<Type> types = resourceRegistryClient.getType(Resource.class, true);
		log.debug("getResourceTypes:",types);
		return types;
		
	}
	
	

	public ArrayList<ResourceTypeDTO> getResourceTypesTree() throws Exception {
		String currentCtx = SecretManagerProvider.instance.get().getContext();
		ResourceRegistryClient resourceRegistryClient= ResourceRegistryClientFactory.create(currentCtx);
		 ModelKnowledge <Type, TypeInformation> modelKnowledge = resourceRegistryClient.getModelKnowledge();
		 Type resourcesType = modelKnowledge.getTypeByName(AccessType.RESOURCE.getName());
	     Tree<Type> typeTree = modelKnowledge.getTree(resourcesType.getAccessType());
	     SimpleTreeMaker treeMaker = new SimpleTreeMaker();
	     typeTree.elaborate(treeMaker);
	     ArrayList<ResourceTypeDTO> orderedNodes = treeMaker.getNodeList();
	     fatherIds = new ArrayList<>();
	     for(ResourceTypeDTO node:orderedNodes) {
	    	 if(!node.getChildren().isEmpty()) {
	    		 fatherIds.add(node.getId());
	    	 }
	     }
	     return cleanTree(orderedNodes);
	}
	
	
	private ArrayList<ResourceTypeDTO> cleanTree(ArrayList<ResourceTypeDTO> ordered) {
		String currentId = null;
		ResourceTypeDTO candidateFatherNode = null;
		for(int i=ordered.size()-1; i>=0; i--) {
			ResourceTypeDTO currentNode = ordered.get(i);
			currentId = currentNode.getId();
			if(fatherIds.contains(currentId)) {
				for(int j=0; j<ordered.size(); j++) {
					Iterator<ResourceTypeDTO> it  = ordered.get(j).getChildren().iterator();
					while(it.hasNext()) {
						candidateFatherNode = it.next(); 
						if(candidateFatherNode.getId().equals(currentId)){
							for(ResourceTypeDTO child: currentNode.getChildren()) {
								candidateFatherNode.addChild(child);
							}
							//TODO: check this
							ordered.remove(currentNode);
							fatherIds.remove(currentId);
						}
					}
				}
			}
		}
		return ordered;
	}
	
	
	/*
	 * per prendere tutti le istanze di un certo tipo (per riempire la tabella)
	 */
	public String getResource(String type, String uid) throws Exception {
		String raw = "";
		String currentCtx = SecretManagerProvider.instance.get().getContext();
		ResourceRegistryClient resourceRegistryClient= ResourceRegistryClientFactory.create(currentCtx);
		resourceRegistryClient.setIncludeMeta(false);
		UUID coso = UUID.fromString(uid);
		raw = resourceRegistryClient.getInstance(type, coso);
		return raw;
	} 
	
	
	/*
	 * Fetches all the resource instances for a given type
	 */
	public List<Resource> getResourceInstances(String resourceType) throws Exception {
		String currentCtx = SecretManagerProvider.instance.get().getContext();
		List<Resource> instancesAsObject;
		log.debug("getResourceInstances : [currentCtx=]",currentCtx);
		ResourceRegistryClient resourceRegistryClient= ResourceRegistryClientFactory.create(currentCtx);
		resourceRegistryClient.setIncludeMeta(true);
		//questo per prendere tutte le istanze (altrimenti il default è 10)
		resourceRegistryClient.setOffset(0);
		resourceRegistryClient.setLimit(-1);
		String instances = resourceRegistryClient.getInstances(resourceType, false);
		instancesAsObject = ElementMapper.unmarshalList(Resource.class, instances);
		return instancesAsObject;
	} 
	


}
