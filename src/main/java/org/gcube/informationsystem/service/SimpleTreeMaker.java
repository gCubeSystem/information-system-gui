package org.gcube.informationsystem.service;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import org.gcube.informationsystem.service.dto.ResourceTypeDTO;
import org.gcube.informationsystem.tree.Node;
import org.gcube.informationsystem.tree.NodeElaborator;
import org.gcube.informationsystem.types.reference.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimpleTreeMaker implements NodeElaborator<Type> {

	private static final Logger log = LoggerFactory.getLogger(SimpleTreeMaker.class);
	
	private ResourceTypeDTO myNode;
	//private SimpleTreeNode[] children;
	//ordered tree node list
	private ArrayList<ResourceTypeDTO> nodeList;
	Map<String, ResourceTypeDTO> nodeMap;
	
	public SimpleTreeMaker() {
		this.nodeList = new ArrayList<ResourceTypeDTO>();
		this.nodeMap = new HashMap<>();
	}
	
	public Map<String, ResourceTypeDTO> getNodeMap() {
		return nodeMap;
	}
	
	public ResourceTypeDTO getSimpleTree() {
		return myNode;
	}
	public ArrayList<ResourceTypeDTO> getNodeList() {
		return nodeList;
	}
	
	@Override
	public void elaborate(Node<Type> node, int level) throws Exception {
		myNode = new ResourceTypeDTO(node.getNodeElement().getName(), node.getNodeElement().getID().toString());
		if(node.getParent()!=null) {
			myNode.setFatherId(node.getParent().getNodeElement().getID().toString()); 
		}else {
			myNode.setFatherId(null);
		}
		
		for(Node<Type> nt:node.getChildrenNodes()) {
			 String name = nt.getNodeElement().getName();
			 String id = nt.getNodeElement().getID().toString();
			 ResourceTypeDTO cn = new ResourceTypeDTO(name, id);
			 cn.setFatherId(nt.getParent().getNodeElement().getID().toString());
			 myNode.getChildren().add(cn);
		 }
		 if(!myNode.getChildren().isEmpty()) {
			 nodeList.add(myNode); 
		 }
		 ////////////////////
		 
		cleanDuplicates(nodeList);
		 
		return;
	}
	
	private void cleanDuplicates(ArrayList<ResourceTypeDTO> allNodes) {
		//Map<String, ResourceTypeDTO> tmpMap = new HashMap<>();
		for(ResourceTypeDTO node:allNodes) {
			if(nodeMap.containsKey(node.getId())) {
				nodeMap.remove(node.getId());
			}else {
				nodeMap.put(node.getId(), node);
				}
		}
	}
	
	  private void visitNodes(ResourceTypeDTO node) {
	        Queue<ResourceTypeDTO> queue = new ArrayDeque<>();
	        queue.add(node);

	        ResourceTypeDTO currentNode;
	        Set<ResourceTypeDTO> alreadyVisited = new HashSet<>();
	        log.debug("Visited nodes: ");
	        while (!queue.isEmpty()) {
	            currentNode = queue.remove();
	            log.debug(currentNode.getName() + " | ");

	            alreadyVisited.add(currentNode);
	            queue.addAll(currentNode.getChildren());
	            queue.removeAll(alreadyVisited);
	        }
	    }

}
