package org.gcube.informationsystem.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.gcube.informationsystem.config.Constants;
import org.gcube.informationsystem.security.AuthoritiesConstants;
import org.gcube.informationsystem.service.dto.AdminUserDTO;
import org.gcube.informationsystem.service.dto.RealmAccessDTO;
import org.gcube.informationsystem.service.dto.ResourceAccessDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.stereotype.Service;

import com.nimbusds.jose.shaded.json.JSONArray;
import com.nimbusds.jose.shaded.json.JSONObject;

/**
 * Service class for managing users.
 */
@Service
public class UserService {

	private static final Logger logger = LoggerFactory.getLogger(UserService.class);

	/**
	 * Returns the user from an OAuth 2.0 login or resource server with JWT.
	 *
	 * @param authToken the authentication token.
	 * @return the user from the authentication.
	 */
	public AdminUserDTO getUserFromAuthentication(AbstractAuthenticationToken authToken) {
		Map<String, Object> attributes;
		if (authToken instanceof JwtAuthenticationToken) {
			logger.debug("AuthToken is a JwtAuthenticationToken");
			attributes = ((JwtAuthenticationToken) authToken).getTokenAttributes();
		} else if (authToken instanceof OAuth2AuthenticationToken) {
			logger.debug("AuthToken is a OAuth2AuthenticationToken");
			OAuth2AuthenticationToken oauth2Token = ((OAuth2AuthenticationToken) authToken);
			attributes = oauth2Token.getPrincipal().getAttributes();
		} else {
			throw new IllegalArgumentException("AuthenticationToken is not OAuth2 or JWT!");
		}
		logger.debug("Attributes: {}", attributes);
		AdminUserDTO user = getUser(attributes);
		Set<String> authorities= new HashSet<>();
		authorities.add(AuthoritiesConstants.ADMIN);
		user.setAuthorities(authorities);
		//user.setAuthorities(
		//   authToken.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toSet()));
		return user;
	}

	private static AdminUserDTO getUser(Map<String, Object> details) {
		AdminUserDTO user = new AdminUserDTO();
		Boolean activated = Boolean.TRUE;
		String sub = String.valueOf(details.get("sub"));
		String username = null;
		if (details.get("preferred_username") != null) {
			username = ((String) details.get("preferred_username")).toLowerCase();
		}
		// handle resource server JWT, where sub claim is email and uid is ID
		if (details.get("uid") != null) {
			user.setId((String) details.get("uid"));
			user.setLogin(sub);
		} else {
			user.setId(sub);
		}
		if (username != null) {
			user.setLogin(username);
		} else if (user.getLogin() == null) {
			user.setLogin(user.getId());
		}
		if (details.get("given_name") != null) {
			user.setFirstName((String) details.get("given_name"));
		} else if (details.get("name") != null) {
			user.setFirstName((String) details.get("name"));
		}
		if (details.get("family_name") != null) {
			user.setLastName((String) details.get("family_name"));
		}
		if (details.get("email_verified") != null) {
			activated = (Boolean) details.get("email_verified");
		}
		if (details.get("email") != null) {
			user.setEmail(((String) details.get("email")).toLowerCase());
		} else if (sub.contains("|") && (username != null && username.contains("@"))) {
			// special handling for Auth0
			user.setEmail(username);
		} else {
			user.setEmail(sub);
		}
		if (details.get("langKey") != null) {
			user.setLangKey((String) details.get("langKey"));
		} else if (details.get("locale") != null) {
			// trim off country code if it exists
			String locale = (String) details.get("locale");
			if (locale.contains("_")) {
				locale = locale.substring(0, locale.indexOf('_'));
			} else if (locale.contains("-")) {
				locale = locale.substring(0, locale.indexOf('-'));
			}
			user.setLangKey(locale.toLowerCase());
		} else {
			// set langKey to default if not specified by IdP
			user.setLangKey(Constants.DEFAULT_LANGUAGE);
		}
		if (details.get("picture") != null) {
			user.setImageUrl((String) details.get("picture"));
		}

		if (details.get("realm_access") != null) {
			//com.nimbusds.jose.shaded.json.
			JSONObject realmAccessJSON=(JSONObject)details.get("realm_access");
			JSONArray rolesJSON=(JSONArray)realmAccessJSON.get("roles");
			ArrayList<String> roles=new ArrayList<>();
			for(int i=0;i<rolesJSON.size();i++) {
				roles.add((String)rolesJSON.get(i));
			}
			
			RealmAccessDTO realmAccessDTO=new RealmAccessDTO(roles);
			user.setRealmAccessDTO(realmAccessDTO);
		}

		if (details.get("resource_access") != null) {
			JSONObject resourceAccessJSON=(JSONObject)details.get("resource_access");
			LinkedHashMap<String, ArrayList<String>> resourceAccess=new LinkedHashMap<>();
			for(String key: resourceAccessJSON.keySet()) {
				JSONObject valueJSON=(JSONObject)resourceAccessJSON.get(key);
				JSONArray rolesJSON=(JSONArray) valueJSON.get("roles");
				ArrayList<String> roles=new ArrayList<>();
				for(int i=0;i<rolesJSON.size();i++) {
					roles.add((String)rolesJSON.get(i));
				}
				resourceAccess.put(key, roles);
			}
			ResourceAccessDTO resourceAccessDTO=new ResourceAccessDTO(resourceAccess);
			user.setResourceAccessDTO(resourceAccessDTO);
		}

		user.setActivated(activated);
		return user;
	}
}
