package org.gcube.informationsystem.service;

import java.util.ArrayList;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Service;
/*
 * Servizio per layer jhipster
 */

@Service
public class ContextService {

    private static final Logger logger = LoggerFactory.getLogger(ContextService.class);
    
    
    public ArrayList<String> all(){
    	ArrayList<String> scopes=new ArrayList<>();
    	scopes.add("gcube");
    	scopes.add("gcube/devsec");
    	scopes.add("gcube/devsec/devVRE");
    	logger.debug("Scopes found: {}", scopes);
    	
    	Authentication auth=SecurityContextHolder.getContext().getAuthentication();
    	DefaultOidcUser userDetails = (DefaultOidcUser) auth.getPrincipal();
    	logger.debug("********************",userDetails.toString());
    	logger.debug("*****dettagli utenti: {}",userDetails.toString());
    	
    	WebAuthenticationDetails details=(WebAuthenticationDetails)auth.getDetails();
    	logger.debug("*****Provaci: {}",details);
    	//String sessionId=RequestContextHolder.currentRequestAttributes().getSessionId();
    	
    	return scopes;
    }

}
