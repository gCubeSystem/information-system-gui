package org.gcube.informationsystem.config;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.gcube.informationsystem.security.AuthoritiesConstants;
import org.gcube.informationsystem.security.SecurityUtils;
import org.gcube.informationsystem.security.oauth2.AudienceValidator;
import org.gcube.informationsystem.security.oauth2.CustomClaimConverter;
import org.gcube.informationsystem.security.oauth2.JwtGrantedAuthorityConverter;
import org.gcube.informationsystem.web.filter.OAuth2RefreshTokensWebFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authorization.AuthorityAuthorizationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
import org.springframework.security.oauth2.client.InMemoryOAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.endpoint.DefaultAuthorizationCodeTokenResponseClient;
import org.springframework.security.oauth2.client.endpoint.NimbusAuthorizationCodeTokenResponseClient;
import org.springframework.security.oauth2.client.endpoint.OAuth2AccessTokenResponseClient;
import org.springframework.security.oauth2.client.endpoint.OAuth2AuthorizationCodeGrantRequest;
import org.springframework.security.oauth2.client.http.OAuth2ErrorResponseErrorHandler;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserRequest;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserService;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserService;
import org.springframework.security.oauth2.core.DelegatingOAuth2TokenValidator;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.core.OAuth2TokenValidator;
import org.springframework.security.oauth2.core.endpoint.OAuth2AccessTokenResponse;
import org.springframework.security.oauth2.core.http.converter.OAuth2AccessTokenResponseHttpMessageConverter;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.security.oauth2.core.oidc.user.OidcUserAuthority;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.JwtDecoders;
import org.springframework.security.oauth2.jwt.JwtValidators;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.header.writers.ReferrerPolicyHeaderWriter;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.filter.CorsFilter;
import org.zalando.problem.spring.web.advice.security.SecurityProblemSupport;

import tech.jhipster.config.JHipsterProperties;


@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
@Import(SecurityProblemSupport.class)
public class SecurityConfiguration {

	private static final Logger logger = LoggerFactory.getLogger(SecurityConfiguration.class);

	private final JHipsterProperties jHipsterProperties;

	private final CorsFilter corsFilter;

	@Value("${spring.security.oauth2.client.provider.oidc.issuer-uri}")
	private String issuerUri;

	private final SecurityProblemSupport problemSupport;
	
	public SecurityConfiguration(CorsFilter corsFilter, JHipsterProperties jHipsterProperties,
			SecurityProblemSupport problemSupport) {
		this.corsFilter = corsFilter;
		this.problemSupport = problemSupport;
		this.jHipsterProperties = jHipsterProperties;
		
	}

	@Bean
	public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
		// @formatter:off
        http
            .csrf()
            	.csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
        .and()
            .addFilterBefore(corsFilter, CsrfFilter.class)
            .exceptionHandling()
               .authenticationEntryPoint(problemSupport)
               .accessDeniedHandler(problemSupport)
        .and()
            .headers()
                .contentSecurityPolicy(jHipsterProperties.getSecurity().getContentSecurityPolicy())
            .and()
                .referrerPolicy(ReferrerPolicyHeaderWriter.ReferrerPolicy.STRICT_ORIGIN_WHEN_CROSS_ORIGIN)
            .and()
                .permissionsPolicy().policy("camera=(), fullscreen=(self), geolocation=(), gyroscope=(), magnetometer=(), microphone=(), midi=(), payment=(), sync-xhr=()")
            .and()
                .frameOptions().sameOrigin()
        .and()
            .authorizeRequests()
            .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
            .antMatchers("/app/**/*.{js,html}").permitAll()
            .antMatchers("/i18n/**").permitAll()
            .antMatchers("/content/**").permitAll()
            .antMatchers("/swagger-ui/**").permitAll()
            .antMatchers("/test/**").permitAll()
            .antMatchers("/login/oauth2/code/oidc").permitAll()
            .antMatchers("/api/account").permitAll()
            .antMatchers("/api/authenticate").permitAll()
            .antMatchers("/api/auth-info").permitAll()
            .antMatchers("/api/admin/**").hasAuthority(AuthoritiesConstants.ADMIN)
            .antMatchers("/api/**").authenticated()
            .antMatchers("/management/health").permitAll()
            .antMatchers("/management/health/**").permitAll()
            .antMatchers("/management/info").permitAll()
            .antMatchers("/management/prometheus").permitAll()
            .antMatchers("/management/**").hasAuthority(AuthoritiesConstants.ADMIN)
        .and()
            .oauth2Login()
            	//.defaultSuccessUrl("/api/scopes/discovery").userInfoEndpoint(userInfo -> userInfo
			    //    .oidcUserService(this.oidcUserService()))
            //.defaultSuccessUrl(issuerUri, false)
            //.defaultSuccessUrl("/api/login/oauth2/code/oidc")
            //.addFilterAfter(new CustomAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class)
            //.logout(logout -> logout.logoutSuccessHandler(oidcLogoutSuccessHandler()))
            //.tokenEndpoint()
            //.accessTokenResponseClient(accessTokenResponseClient())
        //.and()
            //.oauth2ResourceServer()
                //.jwt()
                //.jwtAuthenticationConverter(authenticationConverter())
                //.and()
            .and()
                .oauth2Client();			
                //.authorizationCodeGrant();
                
        
        return http.build();
        // @formatter:on
	}
	
	
	
	private OAuth2UserService<OidcUserRequest, OidcUser> oidcUserService() {
		final OidcUserService delegate = new OidcUserService();

		return (userRequest) -> {
			// Delegate to the default implementation for loading a user
			OidcUser oidcUser = delegate.loadUser(userRequest);
			
			OAuth2AccessToken accessToken = userRequest.getAccessToken();
			
			Set<GrantedAuthority> mappedAuthorities = new HashSet<>();
			logger.debug("Fetch AccessToken: {}",accessToken.getTokenValue());
			// TODO
			// 1) Fetch the authority information from the protected resource using accessToken
			// 2) Map the authority information to one or more GrantedAuthority's and add it to mappedAuthorities

			// 3) Create a copy of oidcUser but use the mappedAuthorities instead
			oidcUser = new DefaultOidcUser(mappedAuthorities, oidcUser.getIdToken(), oidcUser.getUserInfo());
			logger.debug("OIDCUser: {}",oidcUser);
			
			return oidcUser;
		};
	}
	


	Converter<Jwt, AbstractAuthenticationToken> authenticationConverter() {
		logger.debug("JWT AuthenticationConverter()");
		JwtAuthenticationConverter jwtAuthenticationConverter = new JwtAuthenticationConverter();
		jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(new JwtGrantedAuthorityConverter());
		return jwtAuthenticationConverter;
	}

	/**
	 * Map authorities from "groups" or "roles" claim in ID Token.
	 *
	 * @return a {@link GrantedAuthoritiesMapper} that maps groups from the IdP to
	 *         Spring Security Authorities.
	 */
	@Bean
	public GrantedAuthoritiesMapper userAuthoritiesMapper() {
		return authorities -> {
			logger.debug("Authorities: {}", authorities);
			Set<GrantedAuthority> mappedAuthorities = new HashSet<>();

			authorities.forEach(authority -> {
				// Check for OidcUserAuthority because Spring Security 5.2 returns
				// each scope as a GrantedAuthority, which we don't care about.
				logger.debug("Authority: {}", authority);
				if (authority instanceof OidcUserAuthority) {
					OidcUserAuthority oidcUserAuthority = (OidcUserAuthority) authority;
					logger.debug("TokenValue: {}", oidcUserAuthority.getIdToken().getTokenValue());
					logger.debug("oidcUserAuthority: {}", oidcUserAuthority);
					mappedAuthorities.addAll(
							SecurityUtils.extractAuthorityFromClaims(oidcUserAuthority.getUserInfo().getClaims()));
				}
			});
			return mappedAuthorities;
		};
	}

	@Bean
	JwtDecoder jwtDecoder(ClientRegistrationRepository clientRegistrationRepository,
			RestTemplateBuilder restTemplateBuilder) {
		logger.debug("JWTDecoder: {}", restTemplateBuilder);

		NimbusJwtDecoder jwtDecoder = JwtDecoders.fromOidcIssuerLocation(issuerUri);

		OAuth2TokenValidator<Jwt> audienceValidator = new AudienceValidator(
				jHipsterProperties.getSecurity().getOauth2().getAudience());
		OAuth2TokenValidator<Jwt> withIssuer = JwtValidators.createDefaultWithIssuer(issuerUri);
		OAuth2TokenValidator<Jwt> withAudience = new DelegatingOAuth2TokenValidator<>(withIssuer, audienceValidator);

		jwtDecoder.setJwtValidator(withAudience);
		jwtDecoder.setClaimSetConverter(new CustomClaimConverter(
				clientRegistrationRepository.findByRegistrationId("oidc"), restTemplateBuilder.build()));

		return jwtDecoder;
	}

}
