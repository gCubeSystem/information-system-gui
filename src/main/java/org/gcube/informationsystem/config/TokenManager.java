package org.gcube.informationsystem.config;

import org.gcube.informationsystem.service.dto.UmaResponseDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;



@Component
public class TokenManager {

	private static final Logger log = LoggerFactory.getLogger(TokenManager.class);
	@Value("${spring.security.oauth2.client.provider.oidc.token-uri}")
	private String tokenUri;

	@Autowired
	private OAuth2AuthorizedClientService authorizedClientService;

	

	public String getUmaToken(String context) {
		String accessToken=getAccessToken();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		headers.set("Authorization", "Bearer " + accessToken);
		log.debug("Headers: {}", headers);

		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("grant_type", "urn:ietf:params:oauth:grant-type:uma-ticket");
		params.add("audience", context);
		log.debug("Params: {}", params);

		HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(params, headers);

		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<UmaResponseDTO> response = restTemplate.exchange(tokenUri, HttpMethod.POST, entity, UmaResponseDTO.class);
		UmaResponseDTO umaResponseDTO = response.getBody();
		log.debug("UmaResponseDTO: {}", umaResponseDTO);
		String umaToken=umaResponseDTO.getAccess_token();
		log.debug("UmaToken: {}", umaToken);
		return umaToken;
	}

	public String getAccessToken() {
		OAuth2AuthenticationToken oAuth2AuthenticationToken = (OAuth2AuthenticationToken) SecurityContextHolder
				.getContext().getAuthentication();
		log.debug("OAuth2AuthenticationToken: {}", oAuth2AuthenticationToken);

		OAuth2AuthorizedClient client = authorizedClientService.loadAuthorizedClient(
				oAuth2AuthenticationToken.getAuthorizedClientRegistrationId(), oAuth2AuthenticationToken.getName());
		/*if(client.getAccessToken().getExpiresAt().compareTo(Instant.now())>0) {
			
		}*/
		
		String accessToken = client.getAccessToken().getTokenValue();
		
		log.debug("AccessToken: {}", accessToken);
		return accessToken;
	}
	
	public String getRootContext() {
		String rootContext = "/gcube";
		return rootContext;
	}
	

}
