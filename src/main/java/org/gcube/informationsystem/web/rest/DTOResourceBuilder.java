package org.gcube.informationsystem.web.rest;

import java.util.List;
import java.util.Map;

import org.gcube.informationsystem.service.InformationSystemService;
import org.gcube.informationsystem.service.dto.EServiceDTO;
import org.gcube.informationsystem.service.dto.HostingNodeDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

/*
 * Prende il JSON di un singolo nodo e lo trasforma in oggetto udando JSONPath 
 */
public class DTOResourceBuilder {
	
	private String resType;
	private String resJson;
	private Configuration pathConfiguration;
	
	private static final Logger log = LoggerFactory.getLogger(DTOResourceBuilder.class);
	 
	private static String UID_QUERY = "$['id']";
	private static String LAST_UPDATE_QUERY = "$['metadata'].['lastUpdateTime']";
	//private static String
	
	//hostingNodes JSONPath queries
	private static String HN_DOMAIN_HOST_QUERY = "$..consistsOf[?(@.expectedtype=='IsIdentifiedBy')]['target'][?(@.['expectedtype']=='NetworkingFacet')].['domainName','hostName']";
	private static String HN_MEMORY_QUERY = "$..consistsOf[?(@.['type']=='ConsistsOf' && @.['expectedtype']=='HasPersistentMemory')].['target'].['expectedtype','unit','size','used']";
	private static String HN_STATUS_QUERY = "$..consistsOf[?(@.['target'])]*[?(@.['expectedtype']=='StateFacet')].['value']";
	private static String HN_VOL_MEMORY_QUERY = "$..consistsOf[?(@.['expectedtype']=='HasVolatileMemory')].['target'].['size','unit']";
	
	private static String ES_ENDPOINT_QUERY = "$..consistsOf[*]['target'][?(@.['expectedtype']=='AccessPointFacet')].['endpoint','entryName']";
	private static String ES_STATUS_QUERY = "$..consistsOf[*]['target'][?(@.['expectedtype']=='StateFacet')].['value']";
	private static String ES_NGV_QUERY = "$..consistsOf[*]['target'][?(@.['expectedtype']=='SoftwareFacet')].['name','group','version']";
	
	
	//private static String ES_
	public DTOResourceBuilder(String type) {
		resType = type;
		pathConfiguration = Configuration.builder().options(Option.DEFAULT_PATH_LEAF_TO_NULL).build();
	}

	public void setJson(String resJson) {
		this.resJson = resJson;
	}
	
	EServiceDTO esDto = new EServiceDTO();
	
	public Object build(){
		Object dto = null;
		List<Map<String, Object>> myLeaves = null;
		
		switch (this.resType) {
		case Constants.RES_TYPE_HOSTINGNODE:
			dto = new HostingNodeDTO();
			try {
				String uid= JsonPath.parse(resJson).read(UID_QUERY);
				((HostingNodeDTO) dto).setId(uid);
				log.debug("1..."+uid);
				
				String lastUpd= JsonPath.parse(resJson).read(LAST_UPDATE_QUERY);
				lastUpd  = lastUpd.substring(0,lastUpd.length()-10);
				((HostingNodeDTO) dto).setLastMod(lastUpd);		
				log.debug("2..."+lastUpd);
				
				myLeaves = JsonPath.parse(resJson).read(HN_DOMAIN_HOST_QUERY);
				((HostingNodeDTO) dto).setName(myLeaves.get(0).get("hostName").toString());
				log.debug("3..." + myLeaves.get(0).get("hostName").toString());
				
				myLeaves = JsonPath.parse(resJson).read(HN_MEMORY_QUERY);
				((HostingNodeDTO) dto).setAvMemory(myLeaves.get(0).get("size").toString()+" "+myLeaves.get(0).get("unit").toString());
				log.debug("4..."+myLeaves.get(0).get("size").toString()+" "+myLeaves.get(0).get("unit").toString());
				Object status = null;
				status = JsonPath.parse(resJson).read(HN_STATUS_QUERY);
				status = status.toString().replace("[\"", "").replace("\"]", "");
				log.debug("5..."+status);
				((HostingNodeDTO) dto).setStatus(status.toString());
				
				}catch(Exception e) {
					log.error("Error while building HostingNodeDTO from json");
					e.printStackTrace();
				}
				myLeaves = JsonPath.parse(resJson).read(HN_VOL_MEMORY_QUERY);
				((HostingNodeDTO) dto).setHdSpace(myLeaves.get(0).get("size").toString()+" "+myLeaves.get(0).get("unit").toString());
				
			break;

		case Constants.RES_TYPE_ESERVICE:
			dto = new EServiceDTO();
			try {
				String uid= JsonPath.parse(resJson).read(UID_QUERY);
				((EServiceDTO) dto).setId(uid);
				log.debug("1..."+uid);
				
				String lastUpd= JsonPath.parse(resJson).read(LAST_UPDATE_QUERY);
				lastUpd  = lastUpd.substring(0,lastUpd.length()-10);
				((EServiceDTO) dto).setLastMod(lastUpd);		
				log.debug("2..."+lastUpd);
				
				myLeaves= JsonPath.parse(resJson).read(ES_ENDPOINT_QUERY);
				String ep = myLeaves.get(0).get("endpoint").toString();
				String en = myLeaves.get(0).get("entryName").toString();
				
				((EServiceDTO) dto).setEndpoint(ep);
				((EServiceDTO) dto).setName(en);
				log.debug("3..."+ep);
				log.debug("3..."+en);
				
				myLeaves = JsonPath.parse(resJson).read(ES_NGV_QUERY);
				String name = myLeaves.get(0).get("name").toString();
				String group = myLeaves.get(0).get("group").toString();
				String version = myLeaves.get(0).get("version").toString();
				log.debug("5..."+myLeaves);
				((EServiceDTO) dto).setVersion(version);
				((EServiceDTO) dto).setArtifact(name);
				((EServiceDTO) dto).setGroup(group);
				
				
				myLeaves = JsonPath.parse(resJson).read(ES_STATUS_QUERY);
				String sts = myLeaves.toString().replace("[\"", "").replace("\"]", "");
				log.debug("4..."+sts);
				((EServiceDTO) dto).setStatus(sts);
				

			}catch(Exception e) {
				log.error("Error while building EServiceDTO from json");
				e.printStackTrace();	
			}
			break;
			
		default:
			break;
		}
		return dto; 
		
		
	}
}
