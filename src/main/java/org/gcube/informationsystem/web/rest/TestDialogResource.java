package org.gcube.informationsystem.web.rest;

import java.security.SecureRandom;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.gcube.informationsystem.contexts.reference.entities.Context;
import org.gcube.informationsystem.domain.TestDialogDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriUtils;

import lombok.RequiredArgsConstructor;

/**
 * ScopesDiscoveryResource controller
 */
@RestController
@RequestMapping("/api/testdialog")
@RequiredArgsConstructor
public class TestDialogResource {

	private final Logger log = LoggerFactory.getLogger(TestDialogResource.class);

	private final OAuth2AuthorizedClientService authorizedClientService;

	/**
	 * GET test2
	 */
	@GetMapping("/test2")
	public String test2() {
		log.debug("Request Scope Discovery");
		OAuth2AuthenticationToken oAuth2AuthenticationToken = (OAuth2AuthenticationToken) SecurityContextHolder
				.getContext().getAuthentication();
		log.debug("OAuth2AuthenticationToken: {}", oAuth2AuthenticationToken);

		OAuth2AuthorizedClient client = authorizedClientService.loadAuthorizedClient(
				oAuth2AuthenticationToken.getAuthorizedClientRegistrationId(), oAuth2AuthenticationToken.getName());
		log.debug("AccessToken: {}", client.getAccessToken().getTokenValue());

		/*
		 * String userInfoEndpointUri =
		 * client.getClientRegistration().getProviderDetails().getUserInfoEndpoint().
		 * getUri();
		 * 
		 * if (!StringUtils.isEmpty(userInfoEndpointUri)) { HttpHeaders headers = new
		 * HttpHeaders(); headers.add(HttpHeaders.AUTHORIZATION, "Bearer " +
		 * client.getAccessToken().getTokenValue()); HttpEntity entity = new
		 * HttpEntity("", headers);
		 * 
		 * RestTemplate restTemplate = new RestTemplate(); ResponseEntity<Map> response
		 * = restTemplate.exchange(userInfoEndpointUri, HttpMethod.GET, entity,
		 * Map.class); Map userAttributes = response.getBody();
		 * 
		 * model.addAttribute("name", userAttributes.get("name")); }
		 */

		return "Hello";
	}

	/**
	 * GET test
	 */
	@GetMapping("/test")
	public ResponseEntity<TestDialogDTO> test() {
		log.debug("Called Test");
		SecureRandom rand = new SecureRandom();
		int upperbound = 1000;
		// Generating random values from 0 - 999
		// using nextInt()
		int random1 = rand.nextInt(upperbound);
		log.debug("Test: "+random1);
		
		try {
			TimeUnit.SECONDS.sleep(5);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
		TestDialogDTO testDialogDTO = new TestDialogDTO("Hello");
		return new ResponseEntity<TestDialogDTO>(testDialogDTO, HttpStatus.OK);

	}

}
