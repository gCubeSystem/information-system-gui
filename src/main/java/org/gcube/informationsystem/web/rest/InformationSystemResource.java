package org.gcube.informationsystem.web.rest;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.annotation.Nullable;

import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.common.authorization.utils.manager.SecretManagerProvider;
import org.gcube.informationsystem.config.TokenManager;
import org.gcube.informationsystem.contexts.reference.entities.Context;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.informationsystem.service.InformationSystemService;
import org.gcube.informationsystem.service.dto.ContextDTO;
import org.gcube.informationsystem.service.dto.ResourceTypeDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriUtils;

import lombok.RequiredArgsConstructor;
import tech.jhipster.web.util.HeaderUtil;

/**
 * REST controller for managing the current user's account.
 */
@RestController
@RequestMapping("/api/is")
@RequiredArgsConstructor
public class InformationSystemResource {

	private static final Logger log = LoggerFactory.getLogger(InformationSystemResource.class);

	@Value("${jhipster.clientApp.name}")
	private String applicationName;

	private final TokenManager tokenManager;
	private final InformationSystemService informationSystemService;
	
	private String createUmaToken(String currentContext) {
		log.debug("Request uma token");
		if(currentContext==null || currentContext.isBlank()) {
			currentContext = tokenManager.getRootContext();
		}
		String umaToken = tokenManager.getUmaToken(UriUtils.encode(currentContext, "UTF-8"));
		return umaToken;
	}
	
	@GetMapping("/allcontexts")
	public ResponseEntity<String> allContexts() {
		try {
			String currentContext = SecretManagerProvider.instance.get().getContext();
			informationSystemService.setUma(createUmaToken(currentContext));
			List<ContextDTO> contexts = informationSystemService.getAllContexts();
			ObjectMapper objectMapper = new ObjectMapper();
			String sc = objectMapper.writeValueAsString(contexts);
			return ResponseEntity.ok().body(sc);		
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			return ResponseEntity.noContent()
					.headers(HeaderUtil.createAlert(applicationName, e.getLocalizedMessage(), "")).build();
		}

	}
	
	
	@GetMapping("/querytemplates")
	public ResponseEntity<String> testQueryTemplates(@RequestParam @Nullable String currentContext, @RequestParam String query) {
		try {
			informationSystemService.setUma(createUmaToken(currentContext));
			String res = informationSystemService.executeQueryTemplate(query);
			/*
			ObjectMapper objectMapper = new ObjectMapper();
			String sc = objectMapper.writeValueAsString(res);
			*/
			return ResponseEntity.ok().body(res);		
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			return ResponseEntity.noContent()
					.headers(HeaderUtil.createAlert(applicationName, e.getLocalizedMessage(), "")).build();
		}

	}
	
	@GetMapping("/resourcetypes")
	public ResponseEntity<String> resourceTypes(@RequestParam @Nullable String currentContext) {
		log.debug("Request resource types");
		try {
			informationSystemService.setUma(createUmaToken(currentContext));
			ArrayList<ResourceTypeDTO> treeNode = informationSystemService.getResourceTypesTree();
			ObjectMapper objectMapper = new ObjectMapper();
			String sc = objectMapper.writeValueAsString(treeNode);
			return ResponseEntity.ok().body(sc);
		} catch (Exception e) {
			log.error("****ERROR*************");
			e.printStackTrace();
			log.error(e.getLocalizedMessage(), e);
			return ResponseEntity.noContent()
					.headers(HeaderUtil.createAlert(applicationName, e.getLocalizedMessage(), "")).build();
		}
	}
	
	@GetMapping("/resourcejson")
	public ResponseEntity<String> resourceInstanceJson(@RequestParam @Nullable String currentContext, @RequestParam String resourceType,  @RequestParam String  uid) {
		log.debug("Request resource json");
		try {
			informationSystemService.setUma(createUmaToken(currentContext));
			String rawJson = informationSystemService.getResource(resourceType, uid);
			String sc = rawJson;
			return ResponseEntity.ok().body(sc);
		} catch (Exception e) {
			log.error("****ERROR*************");
			e.printStackTrace();
			log.error(e.getLocalizedMessage(), e);
			return ResponseEntity.noContent()
					.headers(HeaderUtil.createAlert(applicationName, e.getLocalizedMessage(), "")).build();
		}
	}

	/*
	 * ritorna le istanze delle risorse di un certo tipo
	 */
	@GetMapping("/resourceinstances")
	public ResponseEntity<String> resourceInstances(@RequestParam @Nullable String currentContext, @RequestParam String resourceType) {
		log.debug("Request resource instances");
		DTOResourceBuilder dtoBuilder = new DTOResourceBuilder(resourceType);
		List<Resource> resourceImpls = new ArrayList<Resource>();
		ArrayList resultDtos = new ArrayList<>();
		try {
			informationSystemService.setUma(createUmaToken(currentContext));
			resourceImpls = informationSystemService.getResourceInstances(resourceType);
			for(Resource resImpl: resourceImpls) {
				String jsonResource = ElementMapper.marshal(resImpl);
				dtoBuilder.setJson(jsonResource);
				resultDtos.add(dtoBuilder.build());
			}
		}catch(Exception e) {
			log.error("ERROR WHILE FILLING RESOURCEIMPL DTO");
			e.printStackTrace();
		}
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			String sc = objectMapper.writeValueAsString(resultDtos);
			return ResponseEntity.ok().body(sc);
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			return ResponseEntity.noContent()
					.headers(HeaderUtil.createAlert(applicationName, e.getLocalizedMessage(), "")).build();
		}
		

	}
}
