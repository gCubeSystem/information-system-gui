package org.gcube.informationsystem.web.rest;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import org.gcube.informationsystem.config.TokenManager;
import org.gcube.informationsystem.service.UserService;
import org.gcube.informationsystem.service.dto.AdminUserDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriUtils;

import lombok.RequiredArgsConstructor;

/**
 * REST controller for managing the current user's account.
 */
@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class AccountResource {

    private static class AccountResourceException extends RuntimeException {

        private static final long serialVersionUID = 1L;

        private AccountResourceException(String message) {
            super(message);
        }
    }

	
    private final Logger log = LoggerFactory.getLogger(AccountResource.class);

    private final TokenManager umaManager;
    private final UserService userService;    
    
    /**
     * {@code GET  /account} : get the current user.
     *
     * @param principal the current user; resolves to {@code null} if not authenticated.
     * @return the current user.
     * @throws AccountResourceException {@code 500 (Internal Server Error)} if the user couldn't be returned.
     */
    @GetMapping("/account")
    public AdminUserDTO getAccount(Principal principal) {   	
    	if (principal instanceof AbstractAuthenticationToken) {
            return userService.getUserFromAuthentication((AbstractAuthenticationToken) principal);
        } else {
            throw new AccountResourceException("User could not be found");
        }
    }

    /**
     * {@code GET  /authenticate} : check if the user is authenticated, and return its login.
     *
     * @param request the HTTP request.
     * @return the login if the user is authenticated.
     */
    @GetMapping("/authenticate")
    public String isAuthenticated(HttpServletRequest request) {
        log.debug("REST request to check if the current user is authenticated");
        return request.getRemoteUser();
    }
    
  
    @GetMapping("/idtoken")
    public String idtoken(OAuth2AuthenticationToken token) {
        log.debug("REST request to check token: {}",token);
        return token.toString();
    }
    
    @GetMapping("/accesstoken")
    public String accesstoken() {
    	log.debug("Request access token");
    	String accessToken=umaManager.getAccessToken();
        return accessToken;
    }
    

    @GetMapping("/umatoken")
    public String umatoken() {
    	log.debug("Request uma token");
    	String context=UriUtils.encode("/gcube", "UTF-8");
    	String umaToken=umaManager.getUmaToken(context);   	
    	return umaToken;
    }

  
     
}
