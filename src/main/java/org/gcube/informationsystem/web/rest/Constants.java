package org.gcube.informationsystem.web.rest;

public final class Constants {
	
	public static final String RES_GCUBE_SUPERTYPE = "GCubeResource";

	public static final String RES_TYPE_HOSTINGNODE = "HostingNode";
	public static final String RES_TYPE_ESERVICE = "EService";
	
	
	public static final String FACET_NETWORKING = "NetworkingFacet";
	public static final String FACET_STATE = "StateFacet";
	public static final String FACET_MEMORY = "MemoryFacet";
	public static final String FACET_SOFTWARE = "SoftwareFacet";
	public static final String FACET_ACCESSPOINT = "AccessPointFacet";
	
	
	public static final String INSTOF_TYPE_CONS_OF = "ConsistsOf";
	public static final String INSTOF_TYPE_ID_BY = "IsIdentifiedBy";
	public static final String INSTOF_TYPE_VOL_MEM = "HasVolatileMemory";
	public static final String INSTOF_TYPE_PERS_MEM = "HasPersistentMemory";
	
	
	//Constants.INSTOF_TYPE_VOL_MEM
	//Constants.INSTOF_TYPE_PERS_MEM

}
