package org.gcube.informationsystem.web.rest;

import java.util.ArrayList;

import org.gcube.informationsystem.service.ContextService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriUtils;

import lombok.RequiredArgsConstructor;

/**
 * ScopesDiscoveryResource controller
 */
@RestController
@RequestMapping("/api/context")
@RequiredArgsConstructor
public class ContextResource {

    private final Logger log = LoggerFactory.getLogger(ContextResource.class);

    private final ContextService contextService;

    private final OAuth2AuthorizedClientService authorizedClientService;

    
   
    /**
     * GET all
     */
    @GetMapping("/all")
    public ArrayList<String> all() {
    	log.debug("Request Scope Discovery");
    	OAuth2AuthenticationToken oAuth2AuthenticationToken=(OAuth2AuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
    	log.debug("OAuth2AuthenticationToken: {}",oAuth2AuthenticationToken);
    	
    	OAuth2AuthorizedClient client = authorizedClientService
                  .loadAuthorizedClient(oAuth2AuthenticationToken.getAuthorizedClientRegistrationId(), oAuth2AuthenticationToken.getName());
    	log.debug("AccessToken: {}",client.getAccessToken().getTokenValue());
    	
        /*String userInfoEndpointUri = client.getClientRegistration().getProviderDetails().getUserInfoEndpoint().getUri();

          if (!StringUtils.isEmpty(userInfoEndpointUri)) {
              HttpHeaders headers = new HttpHeaders();
              headers.add(HttpHeaders.AUTHORIZATION, "Bearer " + client.getAccessToken().getTokenValue());
              HttpEntity entity = new HttpEntity("", headers);

              RestTemplate restTemplate = new RestTemplate();
              ResponseEntity<Map> response = restTemplate.exchange(userInfoEndpointUri, HttpMethod.GET, entity, Map.class);
              Map userAttributes = response.getBody();

              model.addAttribute("name", userAttributes.get("name"));
          }*/
    	
    	return contextService.all();
    }
        
    @GetMapping("/encode")
    public String encodeContext() {
    	String contextEncoded=UriUtils.encode("/gcube", "UTF-8");
    	return contextEncoded;
    }
    
    @GetMapping("/decode")
    public String decodeContext() {
    	String contextEncoded=UriUtils.decode("%2Fgcube", "UTF-8");
    	return contextEncoded;
    }
    
}
