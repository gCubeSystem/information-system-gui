package org.gcube.informationsystem.web.rest;

import org.gcube.informationsystem.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing the current user's login.
 */
@RestController
@RequestMapping("/login")
public class LoginResource {

	  private static class LoginResourceException extends RuntimeException {

	        private static final long serialVersionUID = 1L;

	        private LoginResourceException(String message) {
	            super(message);
	        }
	    }

	    private final Logger log = LoggerFactory.getLogger(AccountResource.class);

	    private final UserService userService;

	    public LoginResource(UserService userService) {
	        this.userService = userService;
	    }
	   
	    @GetMapping("/oauth2/code/oidc")
	    public String login(@RequestParam("code") String code) {
	    	log.debug("REST request OAUTH2 code authorization: {}",code);
	    	return code;
	    }
	
}
