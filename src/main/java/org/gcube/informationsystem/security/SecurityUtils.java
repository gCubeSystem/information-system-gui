package org.gcube.informationsystem.security;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;

/**
 * Utility class for Spring Security.
 */
public final class SecurityUtils {

	private static final Logger logger = LoggerFactory.getLogger(SecurityUtils.class);

	public static final String CLAIMS_NAMESPACE = "https://www.jhipster.tech/";

	private SecurityUtils() {
	}

	/**
	 * Get the login of the current user.
	 *
	 * @return the login of the current user.
	 */
	public static Optional<String> getCurrentUserLogin() {
		SecurityContext securityContext = SecurityContextHolder.getContext();
		return Optional.ofNullable(extractPrincipal(securityContext.getAuthentication()));
	}

	private static String extractPrincipal(Authentication authentication) {
		logger.debug("extractPrincipal: {}", authentication);
		if (authentication == null) {
			return null;
		} else if (authentication.getPrincipal() instanceof UserDetails) {
			UserDetails springSecurityUser = (UserDetails) authentication.getPrincipal();
			return springSecurityUser.getUsername();
		} else if (authentication instanceof JwtAuthenticationToken) {
			return (String) ((JwtAuthenticationToken) authentication).getToken().getClaims().get("preferred_username");
		} else if (authentication.getPrincipal() instanceof DefaultOidcUser) {
			Map<String, Object> attributes = ((DefaultOidcUser) authentication.getPrincipal()).getAttributes();
			if (attributes.containsKey("preferred_username")) {
				return (String) attributes.get("preferred_username");
			}
		} else if (authentication.getPrincipal() instanceof String) {
			return (String) authentication.getPrincipal();
		}
		return null;
	}

	/**
	 * Check if a user is authenticated.
	 *
	 * @return true if the user is authenticated, false otherwise.
	 */
	public static boolean isAuthenticated() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return authentication != null
				&& getAuthorities(authentication).noneMatch(AuthoritiesConstants.ANONYMOUS::equals);
	}

	/**
	 * Checks if the current user has any of the authorities.
	 *
	 * @param authorities the authorities to check.
	 * @return true if the current user has any of the authorities, false otherwise.
	 */
	public static boolean hasCurrentUserAnyOfAuthorities(String... authorities) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return (authentication != null && getAuthorities(authentication)
				.anyMatch(authority -> Arrays.asList(authorities).contains(authority)));
	}

	/**
	 * Checks if the current user has none of the authorities.
	 *
	 * @param authorities the authorities to check.
	 * @return true if the current user has none of the authorities, false
	 *         otherwise.
	 */
	public static boolean hasCurrentUserNoneOfAuthorities(String... authorities) {
		return !hasCurrentUserAnyOfAuthorities(authorities);
	}

	/**
	 * Checks if the current user has a specific authority.
	 *
	 * @param authority the authority to check.
	 * @return true if the current user has the authority, false otherwise.
	 */
	public static boolean hasCurrentUserThisAuthority(String authority) {
		return hasCurrentUserAnyOfAuthorities(authority);
	}

	private static Stream<String> getAuthorities(Authentication authentication) {
		logger.debug("getAuthorities: {}", authentication);
		Collection<? extends GrantedAuthority> authorities = authentication instanceof JwtAuthenticationToken
				? extractAuthorityFromClaims(((JwtAuthenticationToken) authentication).getToken().getClaims())
				: authentication.getAuthorities();
		return authorities.stream().map(GrantedAuthority::getAuthority);
	}

	public static List<GrantedAuthority> extractAuthorityFromClaims(Map<String, Object> claims) {
		logger.debug("extractAuthorityFromClaims: {}", claims);
		return mapRolesToGrantedAuthorities(getRolesFromClaims(claims));
	}

	
	@SuppressWarnings("unchecked")
	private static Collection<String> getRolesFromClaims(Map<String, Object> claims) {
		logger.debug("getRolesFromClaims(): {}", claims);
		// return (Collection<String>) claims.getOrDefault(
		// "groups",
		// claims.getOrDefault("roles", claims.getOrDefault(CLAIMS_NAMESPACE + "roles",
		// new ArrayList<>()))
		// );
		if (claims.get("resource_access") != null) {
			logger.debug("ResourceAccess: {}",claims.get("resource_access").getClass());
			Map<String,Object> scope=(Map<String,Object>)claims.get("resource_access");
			ArrayList<String> roles = new ArrayList<>();
			for (String key : scope.keySet()) {
				logger.info("Default Scope: {}",key);
				roles=((Map<String, ArrayList<String>>)scope.get(key)).get("roles");
				break;
			}
			logger.info("D4Science Roles: {}",roles);
			return roles;
		} else {
			logger.debug("D4Science Roles: {}");
			return new ArrayList<String>();
		}

	}

	private static List<GrantedAuthority> mapRolesToGrantedAuthorities(Collection<String> roles) {
		logger.debug("mapRolesToGrantedAuthorities(): {}", roles);
		List<GrantedAuthority> ga = new ArrayList<>();
		boolean isAdmin = false;
		for (String role : roles) {
			if (role.compareTo("Member") == 0) {
				isAdmin = true;
				break;
			}
		}
		if (isAdmin) {
			ga.add(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN));
		} else {
			ga.add(new SimpleGrantedAuthority(AuthoritiesConstants.USER));

		}
		logger.info("GrantedAuthority: {}",ga);
		return ga;
	}
}
