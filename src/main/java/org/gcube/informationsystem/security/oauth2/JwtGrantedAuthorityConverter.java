package org.gcube.informationsystem.security.oauth2;

import java.util.Collection;

import org.gcube.informationsystem.security.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Component;

@Component
public class JwtGrantedAuthorityConverter implements Converter<Jwt, Collection<GrantedAuthority>> {

	private static final Logger logger = LoggerFactory.getLogger(JwtGrantedAuthorityConverter.class);
	public JwtGrantedAuthorityConverter() {
        logger.debug("New JwtGrantedAuthorityConverter: created");
    }

    @Override
    public Collection<GrantedAuthority> convert(Jwt jwt) {
    	logger.debug("convert(): {}",jwt);
        return SecurityUtils.extractAuthorityFromClaims(jwt.getClaims());
    }
}
