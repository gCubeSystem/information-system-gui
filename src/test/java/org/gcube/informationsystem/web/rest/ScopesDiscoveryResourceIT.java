package org.gcube.informationsystem.web.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.gcube.informationsystem.IntegrationTest;
import org.gcube.informationsystem.service.ContextService;
import org.gcube.informationsystem.web.rest.ContextResource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 * Test class for the ScopesDiscoveryResource REST controller.
 *
 * @see ContextResource
 */
@IntegrationTest
class ScopesDiscoveryResourceIT {

	@Autowired
    private MockMvc restMockMvc;

    @MockBean
	OAuth2AuthorizedClientService authorizedClientService;
    
    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        ContextService scopesDiscoveryService=new ContextService();
        ContextResource scopesDiscoveryResource = new ContextResource(scopesDiscoveryService,authorizedClientService);
        restMockMvc = MockMvcBuilders.standaloneSetup(scopesDiscoveryResource).build();
    }

    /**
     * Test discovery
     */
    @Test
    void testDiscovery() throws Exception {
        restMockMvc.perform(get("/api/scopes/discovery")).andExpect(status().isOk());
    }
}
