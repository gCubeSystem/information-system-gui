package org.gcube.informationsystem.service;

import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.client.ResourceRegistryClient;
import org.gcube.informationsystem.resourceregistry.client.ResourceRegistryClientFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledIf;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@EnabledIf("false")
public class ResourceImplTreeTest {
	
	private static Logger logger = LoggerFactory.getLogger(ResourceImplTreeTest.class);

protected ResourceRegistryClient resourceRegistryClient;
	
@BeforeEach
public void getResourceRegistry() {
	//per la radice
	ResourceRegistryClient resourceRegistryClient= ResourceRegistryClientFactory.create();
	resourceRegistryClient.setIncludeMeta(true);
}

@Test
public void testGetAllHostingNodes() throws ResourceRegistryException {
	String res = resourceRegistryClient.runQueryTemplate("IS_Monitoring-All-HostingNode");
	System.out.println("******HOSTING_NODES******");
	System.out.println(res);
}

@Test
public void testGetAllEServices() throws ResourceRegistryException {
	String res = resourceRegistryClient.runQueryTemplate("IS_Monitoring-All-EService");
	System.out.println("******E_SERVICES******");
	System.out.println(res);
}



}
